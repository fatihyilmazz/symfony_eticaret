<?php

namespace App\Controller\Frontend;

use App\Entity\Cart;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Orders;
use App\Entity\User;
use App\Repository\CartRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductsRepository;
use App\Repository\UserRepository;
use App\Services\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/user", name="user.")
 */
class OrderController extends AbstractController
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * OrderController constructor.
     * @param OrderRepository $orderRepository
     * @param UserRepository $userRepository
     * @param OrderService $orderService
     */
    public function __construct(OrderRepository $orderRepository, UserRepository $userRepository, OrderService $orderService)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->orderService = $orderService;
    }

    /**
     * @Route("/order/create", name="order.create")
     * @return Response
     */
    public function create()
    {
        return $this->render('home/order.html.twig');
    }

    /**
     * @Route("/order/store", name="order.store")
     * @param Request $request
     * @param CartRepository $cartRepository
     * @param ProductsRepository $productsRepository
     * @param UserInterface $user
     * @return Response
     */
    public function store(Request $request, CartRepository $cartRepository,ProductsRepository $productsRepository, UserInterface $user)
    {
        $cart = $cartRepository->findBy([
            'user' => $user->getId(),
            'status' => 0
        ]);

        if (!$cart) {
            throw $this->createNotFoundException('Ürün Bulunamadı');
        }

        $user = $this->userRepository->find($user->getId());

        $entityManager = $this->getDoctrine()->getManager();

        $order = new Orders();
        $order->setUser($user);
        $order->setAddress($request->get('address'));

        $entityManager->persist($order);

        $entityManager->flush();

        foreach ($cart as $item) {
            $order_items = new OrderItem();
            $order_items->setQuantity($item->getQuantity());
            $order_items->setProduct($item->getProduct());
            $order_items->setMainOrder($order);
            $order_items->setTotalPrice($item->getTotalPrice());

            $item->setStatus(1);

            $entityManager->persist($order_items);
            $entityManager->persist($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home');
    }
}
