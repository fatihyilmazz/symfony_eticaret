<?php

namespace App\Controller\Frontend;

use App\Repository\ProductsRepository;
use App\Services\ProductsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    protected $productsRepository;

    protected $productsService;

    public function __construct(ProductsRepository $productsRepository,ProductsService $productsService)
    {
        $this->productsRepository = $productsRepository;
        $this->productsService = $productsService;
    }

    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $products = $this->productsService->getProducts();

        return $this->render('home/index.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("/product/detail/{product_id}", name="product.detail")
     */
    public function detail($product_id)
    {

    }
}
