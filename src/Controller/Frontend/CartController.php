<?php

namespace App\Controller\Frontend;

use App\Entity\Cart;
use App\Entity\User;
use App\Repository\CartRepository;
use App\Repository\ProductsRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/user", name="user.")
 */
class CartController extends AbstractController
{
    protected $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    /**
     * @Route("/cart", name="cart")
     * @param UserInterface $user
     * @return Response
     */
    public function index(UserInterface $user)
    {
        $cart = $this->cartRepository->findBy([
           'user' => $user->getId(),
            'status' => 0
        ]);

        return $this->render('home/cart.html.twig', [
            'cart' => $cart
        ]);
    }

    /**
     * @Route("/add-product-cart/{product_id}", name="cart.add_product")
     * @param $product_id
     * @param ProductsRepository $productsRepository
     * @param UserRepository $userRepository
     * @param UserInterface $user
     * @return Response
     */
    public function addProduct($product_id, ProductsRepository $productsRepository, UserRepository $userRepository, UserInterface $user)
    {
        $product = $productsRepository->find($product_id);

        $user = $userRepository->find($user->getId());

        if (!$product) {
            throw $this->createNotFoundException('Ürün Bulunamadı');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $cart = $this->cartRepository->findBy([
            'user' => $user->getId(),
            'product' => $product_id
        ]);
        if ($cart) {
            //TODO: find by array dönüyor object bak

            $cart[0]->setQuantity($cart[0]->getQuantity() + 1);
            $cart[0]->setTotalPrice($cart[0]->getQuantity()* $product->getPrice());

            $entityManager->persist($product);

            $entityManager->flush();

            return $this->redirectToRoute('user.cart');
        }

        $cart = new Cart();

        $cart->setUser($user);
        $cart->setProduct($product);
        $cart->setQuantity(1);
        $cart->setStatus(1);
        $cart->setTotalPrice($product->getPrice());

        $entityManager->persist($cart);

        $entityManager->flush();

        return $this->redirectToRoute('user.cart');
    }
}
