<?php

namespace App\Controller\Admin;

use App\Entity\Products;
use App\Repository\ProductsRepository;
use App\Services\ProductsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin.")
 */
class ProductController extends AbstractController
{
    protected $productsService;

    public function __construct(ProductsService $productsService)
    {
        $this->productsService = $productsService;
    }

    /**
     * @Route("/products", name="products")
     * @param ProductsRepository $productsRepository
     * @return Response
     */
    public function index(ProductsRepository $productsRepository)
    {
        $products = $productsRepository->findAll();

        return $this->render('admin/products/index.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route ("/product/new", name="products.new")
     * Method ({"GET"})
     * @return Response
     */
    public function create()
    {
        return $this->render('admin/products/create.html.twig');
    }

    /**
     * @Route ("/product/store", name="products.store")
     * Method ({"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function store(Request $request,ValidatorInterface $validator)
    {
        $product = new Products();

        $errors = $validator->validate($product);

        if (count($errors) > 0) {
            return $this->render('register/index.html.twig', [
                'errors' => $errors
            ]);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $product->setTitle($request->get('title'));
        $product->setDescription($request->get('description'));
        $product->setStock($request->get('stock'));
        $product->setPrice($request->get('price'));
        $product->setOnSale($request->get('on_sale'));

        $image = $this->productsService->saveImage($request);

        $product->setImage($image);

        $entityManager->persist($product);

        $entityManager->flush();

        return $this->redirectToRoute('admin.products');
    }

    /**
     * @Route ("/product/update/{product_id}", name="products.update")
     * Method ({"POST"})
     * @param $product_id
     * @param Request $request
     * @param ProductsRepository $productsRepository
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function update($product_id,Request $request,ProductsRepository $productsRepository,ValidatorInterface $validator)
    {
        $product = new Products();

        $errors = $validator->validate($product);

        if (count($errors) > 0) {
            return $this->render('register/index.html.twig', [
                'errors' => $errors
            ]);
        }

        $product = $productsRepository->find($product_id);

        if (!$product) {
            throw $this->createNotFoundException('Ürün Bulunamadı');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $product->setTitle($request->get('title'));
        $product->setDescription($request->get('description'));
        $product->setStock($request->get('stock'));
        $product->setOnSale($request->get('on_sale'));

        $entityManager->persist($product);

        $entityManager->flush();

        return $this->redirectToRoute('admin.products');
    }


    /**
     * @Route("/product/show/{product_id}", name="product.show")
     * @param $product_id
     * @param ProductsRepository $productsRepository
     * @return Response
     */
    public function show($product_id,ProductsRepository $productsRepository)
    {
        $product = $productsRepository->find($product_id);

        return $this->render('admin/products/update.html.twig', [
            'product' => $product
        ]);
    }
}
