<?php

namespace App\Controller\Admin;

use App\Entity\Products;
use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductsRepository;
use App\Services\ProductsService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin.")
 */
class OrderController extends AbstractController
{
    protected $orderRepository;

    protected $orderItemRepository;

    public function __construct(OrderRepository $orderRepository, OrderItemRepository $orderItemRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
    }

    /**
     * @Route("/orders", name="orders")
     * @return Response
     */
    public function index()
    {
        $orders = $this->orderRepository->findAll();

        return $this->render('admin/orders/index.html.twig', [
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/order/show/{order_id}", name="order.show")
     * @param $order_id
     * @return Response
     */
    public function show($order_id)
    {
        $order = $this->orderRepository->find($order_id);

        $order_items = $this->orderItemRepository->findBy([
            'main_order' => $order_id
        ]);

        return $this->render('admin/orders/list.html.twig', [
            'order' => $order,
            'order_items' => $order_items
        ]);
    }

    /**
     * @Route("/order/approve/{order_id}", name="order.approve")
     * @param $order_id
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function approve($order_id,EntityManagerInterface $entityManager)
    {
        $order = $this->orderRepository->find($order_id);
        $order->setStatus(1);

        $entityManager->flush();

        return $this->redirectToRoute('admin.orders');
    }
}
