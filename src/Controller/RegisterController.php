<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register()
    {
        return $this->render('register/index.html.twig');
    }

    /**
     * @Route("/register_user", name="register.user")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ValidatorInterface $validator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator)
    {
        $user = new User();

        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            return $this->render('register/index.html.twig', [
                'errors' => $errors
            ]);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $user->setEmail($request->get('email'));
        $user->setPassword($passwordEncoder->encodePassword($user, $request->get('password')));

        $entityManager->persist($user);

        $entityManager->flush();

        return $this->redirect($this->generateUrl('app_login'));
    }
}
